package com.arias.client.controller;

import com.arias.client.DTO.ClienteDTO;
import com.arias.client.Entities.ClienteIdentity;
import com.arias.client.Entities.Clientes;
import com.arias.client.Services.clientService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@WebAppConfiguration
@AutoConfigureMockMvc
class clienteControllerTest {

    @MockBean
    private clientService clientService;

    @Autowired
    private ModelMapper modelMapper;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Mock
    private ClienteDTO clienteDTO;

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    void getAllClients() throws Exception {
        List<Clientes>  clientesList = new ArrayList<>();
        Clientes client = new Clientes();
        client.setClienteIdentity(new ClienteIdentity("CC", 1234567L));
        client.setPrimerNombre("Pepe");
        client.setSegundoNombre("Lalo");
        client.setPrimerApellido("Perez");
        client.setSegundoApellido("Lopez");
        client.setEmail("pepelalo.perezlopez@mymail.com");
        client.setNumeroCelular(31234567L);
        client.setFechaNacimiento(new Date(System.currentTimeMillis()));
        client.setDireccionResidencia("LA Street 123");
        client.setPlanPreferido(2L);
        clientesList.add(client);

        when(clientService.findAllClients()).thenReturn(clientesList);

        mockMvc.perform(get("/api/v1/clientes/"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$[0].cliente_id.tipo_Identificacion").value("CC"))
                .andExpect(jsonPath("$[0].cliente_id.numero_Identificacion").value(1234567))
                .andExpect(jsonPath("$[0].primer_nombre").value("Pepe"))
                .andExpect(jsonPath("$[0].segundo_nombre").value("Lalo"))
                .andExpect(jsonPath("$[0].primer_apellido").value("Perez"))
                .andExpect(jsonPath("$[0].segundo_apellido").value("Lopez"))
                .andExpect(jsonPath("$[0].direccion_residencia").value("LA Street 123"))
                .andExpect(jsonPath("$[0].numero_celular").value(31234567))
                .andExpect(jsonPath("$[0].email").value("pepelalo.perezlopez@mymail.com"))
                .andExpect(jsonPath("$[0].fecha_nacimiento").value("2021-06-04"))
                .andExpect(jsonPath("$[0].plan_preferido").value(2));
    }

    @Test
    void createCliente() throws Exception {

        Clientes clienteToService = convertToEntity(clienteDTO);

        Clientes client = new Clientes();
        client.setClienteIdentity(new ClienteIdentity("CC", 1234567L));
        client.setPrimerNombre("Pepe");
        client.setSegundoNombre("Lalo");
        client.setPrimerApellido("Perez");
        client.setSegundoApellido("Lopez");
        client.setEmail("pepelalo.perezlopez@mymail.com");
        client.setNumeroCelular(31234567L);
        client.setFechaNacimiento(new Date(System.currentTimeMillis()));
        client.setDireccionResidencia("LA Street 123");
        client.setPlanPreferido(2L);

        when(clientService.createClientes(clienteToService)).thenReturn(client);

        mockMvc.perform(post("/api/v1/clientes/")
                .contentType("application/json")
                .content("{\"cliente_id\":{\"tipo_Identificacion\":\"CC\",\"numero_Identificacion\":1234567},\"primer_nombre\":\"Pepe\",\"segundo_nombre\":\"Lalo\",\"primer_apellido\":\"Perez\",\"segundo_apellido\":\"Lopez\",\"direccion_residencia\":\"LA Street 123\",\"numero_celular\":31234567,\"email\":\"pepelalo.perezlopez@mymail.com\",\"fecha_nacimiento\":\"2021-06-04\",\"plan_preferido\":2}"))
                .andDo(print()).andExpect(status().is(201))
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.cliente_id.tipo_Identificacion").value("CC"))
                .andExpect(jsonPath("$.cliente_id.numero_Identificacion").value(1234567))
                .andExpect(jsonPath("$.primer_nombre").value("Pepe"))
                .andExpect(jsonPath("$.segundo_nombre").value("Lalo"))
                .andExpect(jsonPath("$.primer_apellido").value("Perez"))
                .andExpect(jsonPath("$.segundo_apellido").value("Lopez"))
                .andExpect(jsonPath("$.direccion_residencia").value("LA Street 123"))
                .andExpect(jsonPath("$.numero_celular").value(31234567))
                .andExpect(jsonPath("$.email").value("pepelalo.perezlopez@mymail.com"))
                .andExpect(jsonPath("$.fecha_nacimiento").value("2021-06-04"))
                .andExpect(jsonPath("$.plan_preferido").value(2));
    }

    @Test
    void updateCliente() throws Exception {

        Clientes clienteToService = convertToEntity(clienteDTO);

        Clientes client = new Clientes();
        client.setClienteIdentity(new ClienteIdentity("CC", 1234567L));
        client.setPrimerNombre("Pepe");
        client.setSegundoNombre("Lalo");
        client.setPrimerApellido("Perez");
        client.setSegundoApellido("Lopez");
        client.setEmail("pepelalo.perezlopez@mymail.com");
        client.setNumeroCelular(31234567L);
        client.setFechaNacimiento(new Date(System.currentTimeMillis()));
        client.setDireccionResidencia("LA Street 123");
        client.setPlanPreferido(2L);

        when(clientService.updateClientes(clienteToService)).thenReturn(true);

        mockMvc.perform(put("/api/v1/clientes/")
                .contentType("application/json")
                .content("{\"cliente_id\":{\"tipo_Identificacion\":\"CC\",\"numero_Identificacion\":1234567},\"primer_nombre\":\"Pepe\",\"segundo_nombre\":\"Lalo\",\"primer_apellido\":\"Perez\",\"segundo_apellido\":\"Lopez\",\"direccion_residencia\":\"LA Street 123\",\"numero_celular\":31234567,\"email\":\"pepelalo.perezlopez@mymail.com\",\"fecha_nacimiento\":\"2021-06-04\",\"plan_preferido\":2}"))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void deletePersona() throws Exception {
        Clientes clienteToService = convertToEntity(clienteDTO);

        Clientes client = new Clientes();
        client.setClienteIdentity(new ClienteIdentity("CC", 1234567L));
        client.setPrimerNombre("Pepe");
        client.setSegundoNombre("Lalo");
        client.setPrimerApellido("Perez");
        client.setSegundoApellido("Lopez");
        client.setEmail("pepelalo.perezlopez@mymail.com");
        client.setNumeroCelular(31234567L);
        client.setFechaNacimiento(new Date(System.currentTimeMillis()));
        client.setDireccionResidencia("LA Street 123");
        client.setPlanPreferido(2L);

        when(clientService.deleteClientes(clienteToService)).thenReturn(true);

        mockMvc.perform(delete("/api/v1/clientes/")
                .contentType("application/json")
                .content("{\"cliente_id\":{\"tipo_Identificacion\":\"CC\",\"numero_Identificacion\":1234567},\"primer_nombre\":\"Pepe\",\"segundo_nombre\":\"Lalo\",\"primer_apellido\":\"Perez\",\"segundo_apellido\":\"Lopez\",\"direccion_residencia\":\"LA Street 123\",\"numero_celular\":31234567,\"email\":\"pepelalo.perezlopez@mymail.com\",\"fecha_nacimiento\":\"2021-06-04\",\"plan_preferido\":2}"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(content().string("Borrado - tipo_Identificacion : CC, numero_Identificacion : 1234567"));

    }

    private ClienteDTO convertToDto(Clientes cliente) {
        ClienteDTO postDto = cliente.customShow(mapToClientDTO);
        return postDto;
    }


    private Clientes convertToEntity(ClienteDTO clienteDTO) throws ParseException {
        return modelMapper.map(clienteDTO, Clientes.class);
    }

    private Function<Clientes, ClienteDTO> mapToClientDTO = p -> ClienteDTO.builder().clienteID(p.getClienteIdentity())
            .primerNombre(p.getPrimerNombre())
            .segundoNombre(p.getSegundoNombre())
            .primerApellido(p.getPrimerApellido())
            .segundoApellido(p.getSegundoApellido())
            .direccionResidencia(p.getDireccionResidencia())
            .numeroCelular(p.getNumeroCelular())
            .email(p.getEmail())
            .fechaNacimiento(Timestamp.valueOf(p.getFechaNacimiento().toLocalDate().atStartOfDay()))
            .planPreferido(p.getPlanPreferido()).build();
    
}