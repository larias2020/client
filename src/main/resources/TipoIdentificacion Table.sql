CREATE TABLE `users`.`tipo_identificacion` (
 `tipo_identificacion` VARCHAR(2) NOT NULL ,
 `descripcion_identificacion` VARCHAR(20) NOT NULL ,
 PRIMARY KEY (`tipo_identificacion`)
 ) 
 ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_unicode_ci;