package com.arias.client.Repositories;

import com.arias.client.Entities.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * The interface Log repository.
 * @author Luis Arias
 */
@Repository("logRepository")
public interface LogRepository extends JpaRepository<Log, Serializable> {
}
