package com.arias.client.Repositories;

import com.arias.client.Entities.ClienteIdentity;
import com.arias.client.Entities.Clientes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Cliente repo.
 * @author Luis Arias
 */
@Repository
public interface ClientRepo extends JpaRepository<Clientes, ClienteIdentity> {
}
