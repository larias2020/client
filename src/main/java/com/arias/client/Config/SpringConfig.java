package com.arias.client.Config;

import com.arias.client.Services.clientService;
import com.arias.client.Services.impl.clientServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Spring config.
 * @author Luis Arias
 */
@Configuration
public class SpringConfig {
    /**
     * Cliente service client service.
     *
     * @return the persona service
     */
    @Bean
    public clientService clientService() {
        return new clientServiceImpl();
    }

    /**
     * Model mapper model mapper.
     *
     * @return the model mapper
     */
    @Bean
    public ModelMapper modelMapper(){ return new ModelMapper();}
}
