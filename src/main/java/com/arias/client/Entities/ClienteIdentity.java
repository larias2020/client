package com.arias.client.Entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ClienteIdentity  implements Serializable {

    @Getter
    @Setter
    @EqualsAndHashCode.Include
    private String tipo_Identificacion;

    @Getter
    @Setter
    @EqualsAndHashCode.Include
    private Long numero_Identificacion;

    public ClienteIdentity() {

    }

    public ClienteIdentity(String tipo_Identificacion, Long numero_Identificacion) {
        this.tipo_Identificacion = tipo_Identificacion;
        this.numero_Identificacion = numero_Identificacion;
    }
}
