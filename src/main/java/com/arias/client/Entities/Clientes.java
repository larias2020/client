package com.arias.client.Entities;



import com.arias.client.DTO.ClienteDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.util.function.Function;

/**
 * The type Clientes.
 * @author Luis Arias
 */
@Entity
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernate_lazy_initializer", "handler"})
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Clientes {

    @Getter
    @Setter
    @EmbeddedId
    private ClienteIdentity clienteIdentity;

    @Getter
    @Setter
    private String primerNombre;

    @Getter
    @Setter
    private String segundoNombre;

    @Getter
    @Setter
    private String primerApellido;

    @Getter
    @Setter
    private String segundoApellido;

    @Getter
    @Setter
    private String direccionResidencia;

    @Getter
    @Setter
    private Long numeroCelular;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private Date fechaNacimiento;

    @Getter
    @Setter
    private Long planPreferido;

    /**
    @ManyToOne(fetch = FetchType.LAZY)
    @Getter
    @Setter
    @org.hibernate.annotations.ColumnDefault("")
    private Clientes padre;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "padre")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Setter
    private Set<Clientes> hijos;

    /**
     * Gets hijos.
     *
     * @return the hijos

    @JsonIgnore
    public Set<Clientes> getHijos() {
        return hijos;
    }
     * Custom show persona dto.
     *
     * @param fun the fun
     * @return the persona dto
     * */

    public ClienteDTO customShow(Function<Clientes, ClienteDTO> fun){
        return fun.apply(this);
    }

}
