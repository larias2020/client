package com.arias.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * The type Cliente application.
 * @author Luis Arias
 */
@SpringBootApplication
@EnableEurekaClient
public class ClientApplication {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args);
    }

}
