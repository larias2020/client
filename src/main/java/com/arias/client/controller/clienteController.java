package com.arias.client.controller;

import com.arias.client.DTO.ClienteDTO;
import com.arias.client.Entities.Clientes;
import com.arias.client.Entities.ClienteIdentity;
import com.arias.client.Services.clientService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The type Cliente controller.
 * @author Luis Arias
 */
@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api/v1/")
public class clienteController {

    @Autowired
    private clientService clientService;

    @Autowired
    private ModelMapper modelMapper;


    /**
     * Gets all clients.
     * @return the all clients
     */
    @GetMapping("/clientes/")
    public ResponseEntity<List<ClienteDTO>> getAllClients() {
        List<Clientes> clients = clientService.findAllClients();
        List<ClienteDTO> clienteDTOS =  clients.stream().map(mapToClientDTO).collect(Collectors.toList());
        return new ResponseEntity<>(clienteDTOS, HttpStatus.OK);
    }



    /**
     * Creates cliente  dto.
     *
     * @param clienteDTO the persona dto
     * @return the cliente dto
     * @throws ParseException the parse exception
     */
    @PostMapping("/clientes/")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ClienteDTO createCliente(@RequestBody ClienteDTO clienteDTO) throws ParseException {
        Clientes clienteToService = convertToEntity(clienteDTO);
        Clientes clienteFromService = clientService.createClientes(clienteToService);
        return convertToDto(clienteFromService);
    }

    /**
     * Update post.
     *
     * @param clienteDTO the cliente dto
     * @throws ParseException the parse exception
     */
    @PutMapping(value = "/clientes/")
    @ResponseStatus(HttpStatus.OK)
    public void updateCliente(@RequestBody ClienteDTO clienteDTO) throws ParseException {
        Clientes cliente = convertToEntity(clienteDTO);
        clientService.updateClientes(cliente);
    }

    /**
     * Delete cliente response entity.
     *
     * @param clienteDTO type id
     * @return the response entity
     */
    @DeleteMapping(value = "/clientes/")
    @ResponseBody
    public ResponseEntity<String> deletePersona(@RequestBody ClienteDTO clienteDTO) {
        Long ni = clienteDTO.getClienteID().getNumero_Identificacion();
        String ti = clienteDTO.getClienteID().getTipo_Identificacion();
        Clientes cliente = new Clientes();
        cliente.setClienteIdentity(new ClienteIdentity(ti, ni));
        boolean isRemoved = clientService.deleteClientes(cliente);
        if (isRemoved==false) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>( "Borrado - tipo_Identificacion : " + ti +", numero_Identificacion : "+ ni, HttpStatus.OK);
    }


    private ClienteDTO convertToDto(Clientes cliente) {
        ClienteDTO postDto = cliente.customShow(mapToClientDTO);
        return postDto;
    }


    private Clientes convertToEntity(ClienteDTO clienteDTO) throws ParseException {
        return modelMapper.map(clienteDTO, Clientes.class);
    }

    private Function<Clientes, ClienteDTO> mapToClientDTO = p -> ClienteDTO.builder().clienteID(p.getClienteIdentity())
            .primerNombre(p.getPrimerNombre())
            .segundoNombre(p.getSegundoNombre())
            .primerApellido(p.getPrimerApellido())
            .segundoApellido(p.getSegundoApellido())
            .direccionResidencia(p.getDireccionResidencia())
            .numeroCelular(p.getNumeroCelular())
            .email(p.getEmail())
            .fechaNacimiento(Timestamp.valueOf(p.getFechaNacimiento().toLocalDate().atStartOfDay()))
            .planPreferido(p.getPlanPreferido()).build();

}
