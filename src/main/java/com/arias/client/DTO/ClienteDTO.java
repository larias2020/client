package com.arias.client.DTO;

import com.arias.client.Entities.ClienteIdentity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * The type Cliente dto.
 * @author Luis Arias
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties({"hibernate_lazy_initializer", "handler"})
public class ClienteDTO {
    private ClienteIdentity clienteID;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String direccionResidencia;
    private Long numeroCelular;
    private String email;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Timestamp fechaNacimiento;
    private Long planPreferido;
}
