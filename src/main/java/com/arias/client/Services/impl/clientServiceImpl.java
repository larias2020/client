package com.arias.client.Services.impl;

import com.arias.client.Entities.ClienteIdentity;
import com.arias.client.Entities.Clientes;
import com.arias.client.Repositories.ClientRepo;
import com.arias.client.Services.clientService;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
import java.util.Optional;

/**
 * The type Cliente service.
 * @author Luis Arias
 */
public class clientServiceImpl implements clientService {

    @Autowired
    private ClientRepo clientRepo;

    @Override
    public List<Clientes> findAllClients() {
        return clientRepo.findAll();
    }

    @Override
    public Optional<Clientes> findClienteById(ClienteIdentity id) {
        return clientRepo.findById(id);
    }

    @Override
    public Clientes createClientes(Clientes cliente) {
        return clientRepo.save(cliente);
    }

    @Override
    public boolean updateClientes(Clientes cliente){
        if(clientRepo.existsById(cliente.getClienteIdentity())){
            clientRepo.save(cliente);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteClientes(Clientes cliente) {
        if(clientRepo.existsById(cliente.getClienteIdentity())){
            clientRepo.deleteById(cliente.getClienteIdentity());
            return true;
        }
        return false;
    }
}


