package com.arias.client.Services;

import com.arias.client.Entities.ClienteIdentity;
import com.arias.client.Entities.Clientes;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * The interface Cliente service.
 * @author Luis Arias
 */
@Service
public interface clientService {

    /**
     * Encontrar all clients optional.
     *
     *
     * @return the optional
     */
    List<Clientes> findAllClients( );


    /**
     * Encontrar userby id optional.
     *
     * @param clienteIdentity the id id the client
     * @return the optional
     */
    Optional<Clientes> findClienteById(ClienteIdentity clienteIdentity );

    /**
     * Create cliente cliente.
     *
     * @param cliente the cliente
     * @return the cliente
     */
    Clientes createClientes(Clientes cliente);

    /**
     * Update cliente boolean.
     *
     * @param cliente the cliente
     * @return the boolean
     */
    boolean updateClientes(Clientes cliente);

    /**
     * Delete boolean.
     *
     * @param clienteIdentity the id of the client
     * @return the boolean
     */
    boolean deleteClientes(Clientes cliente);
}
