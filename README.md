## Client Application

### Description

This project is intended to expose a set of services to get a list of clients, create a new client, update client information and delete its information.

### Instructions for Running Locally 

This project is a microservice project. Make sure client-preferences project, that is also in my repo, is running properly in HTTP port 8761; that is a Eureka server.

This project is built in spring-boot and runs over HTTP port 8181. You can test it in http://localhost:8181. Username and password values are root.

Make sure you are running a Mysql DB named users before running this project. SQL Scripts to create tables are found in src/main/resources/ project path.

To run this project make sure you can download maven libraries.

Enjoy!